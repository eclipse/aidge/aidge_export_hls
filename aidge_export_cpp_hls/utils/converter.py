import numpy as np
import aidge_core

def numpy_dtype2ctype(dtype):
    if dtype == np.int8:
        return "int8_t"
    elif dtype == np.int16:
        return "int16_t"
    elif dtype == np.int32:
        return "int32_t"
    elif dtype == np.int64:
        return "int64_t"
    elif dtype == np.float32:
        return "float"
    elif dtype == np.float64:
        return "double"
    # Add more dtype mappings as needed
    else:
        raise ValueError(f"Unsupported {dtype} dtype")
    

def aidge_datatype2ctype(datatype):
    if datatype == aidge_core.DataType.Int8:
        return "int8_t"
    elif datatype == aidge_core.DataType.Int32:
        return "int32_t"
    elif datatype == aidge_core.DataType.Int64:
        return "int64_t"
    elif datatype == aidge_core.DataType.Float32:
        return "float"
    elif datatype == aidge_core.DataType.Float64:
        return "double"
    # Add more dtype mappings as needed
    else:
        raise ValueError(f"Unsupported {datatype} aidge datatype")
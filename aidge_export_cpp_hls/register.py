"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

EXPORT_CPP_REGISTRY = {}


def export_cpp_register(*args):
   
    key_list = [arg for arg in args]

    def decorator(operator):
        def wrapper(*args, **kwargs):
            return operator(*args, **kwargs)
        
        for key in key_list:
            EXPORT_CPP_REGISTRY[key] = operator

        return wrapper
    return decorator

def supported_operators():
    return list(EXPORT_CPP_REGISTRY.keys())


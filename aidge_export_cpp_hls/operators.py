import os
import shutil
import numpy as np
from pathlib import Path
from jinja2 import Environment, FileSystemLoader

from aidge_core import ExportNode
from aidge_core.export.code_generation import *
from aidge_export_cpp.utils import ROOT, operator_register
from aidge_export_cpp.utils.converter import numpy_dtype2ctype
from aidge_export_cpp.utils.generation import *

##############################################
################### Utils ####################
##############################################

def get_node_parents(node):
    parents = []
    for parent in node.get_parents():
        if parent.type() != "Producer":
            parents.append(parent)
    return parents

def get_producer_parents(node):
    parents = []
    for parent in node.get_parents():
        if parent.type() == "Producer":
            parents.append(parent)
    return parents


##############################################
############## Export functions ##############
##############################################

def export_params(name:str,
                  array: np.ndarray,
                  filepath:str):

    # Get directory name of the file
    dirname = os.path.dirname(filepath)

    # If directory doesn't exist, create it
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    generate_file(
        filepath,
        str(ROOT / "templates" / "data" / "parameters.jinja"),
        name = name,
        data_t = numpy_dtype2ctype(array.dtype),
        values = array.tolist()
    )


##############################################
################### Actions ##################
##############################################

def set_up_output(name, datatype):
    return f"{datatype}* {name} = ({datatype}*) mem + {name.upper()}_OFFSET;"


##############################################
############## Operators helper ##############
##############################################

@operator_register("Producer")
class ProducerCPP(ExportNode):

    def __init__(self, node):
        super().__init__(node)
        self.constant = self.operator.get_attr("Constant")
        self.values = np.array(self.operator.get_output(0))

        if len(self.values.shape) == 4:
            self.values = np.transpose(self.values, (0, 2, 3, 1))

    def export(self, export_folder:Path, list_configs:list):

        # If not constant, it is a dataprovider
        # and not a parameter provider
        if (self.constant):
            list_configs.append(f"parameters/{self.name}.h")

            # Export in HWC
            export_params(self.name,
                          self.values.reshape(-1),
                          str(export_folder / "parameters" / f"{self.name}.h"))

        return list_configs

    def forward(self, list_actions:list):
        # A Producer does nothing during forward
        return list_actions


@operator_register("ReLU")
class ReLUCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

        self.nb_data = 1
        for i in self.inputs_dims[0]:
            self.nb_data *= i

    def export(self, export_folder:Path, list_configs:list):

        copyfile(str(ROOT / "kernels" / "activation.hpp"),
                 str(export_folder / "include" / "kernels"))

        list_configs.append("kernels/activation.hpp")
        list_configs.append(f"layers/{self.name}.h")
        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "activation_config.jinja"),
            name=self.name,
            nb_data=self.nb_data,
            activation="Rectifier",
            rescaling="NoScaling")

        return list_configs

    def forward(self, list_actions:list):

        if not self.is_last:
            list_actions.append(set_up_output(self.name, "float"))

        list_actions.append(generate_str(
            str(ROOT / "templates" / "kernel_forward" / "activation_forward.jinja"),
            name=self.name,
            input_name=self.inputs[0].name(),
            output_name=self.name
        ))
        return list_actions


@operator_register("Conv")
class ConvCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

        self.kernel = node.get_operator().get_attr("KernelDims")
        self.stride = node.get_operator().get_attr("StrideDims")
        self.dilation = node.get_operator().get_attr("DilationDims")

        # No padding with Conv
        # Use PaddedConv to add padding attribute
        self.padding = [0, 0]

        self.nb_channels = node.get_operator().get_attr("InChannels")
        self.nb_outputs = node.get_operator().get_attr("OutChannels")

        if len(self.inputs_dims[0]) == 4:
            # if dims == [batch, nb_channels, height, width]
            # transform to [nb_channels, height, width]
            self.inputs_dims[0] = self.inputs_dims[0][1:]

        if len(self.outputs_dims[0]) == 4:
            # if dims == [batch, nb_outputs]
            # transform to [nb_outputs, 1, 1]
            self.outputs_dims[0] = self.outputs_dims[0][1:]

    def export(self, export_folder:Path, list_configs:list):

        copyfile(str(ROOT / "kernels" / "convolution.hpp"),
                 str(export_folder / "include" / "kernels"))
        copyfile(str(ROOT / "kernels" / "macs.hpp"),
                 str(export_folder / "include" / "kernels"))
        copyfile(str(ROOT / "kernels" / "activation.hpp"),
                 str(export_folder / "include" / "kernels"))

        list_configs.append("kernels/convolution.hpp")
        list_configs.append(f"layers/{self.name}.h")
        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "convolution_config.jinja"),
            name=self.name,
            input_dims=self.inputs_dims[0],
            output_dims=self.outputs_dims[0],
            kernel=self.kernel,
            stride=self.stride,
            padding=self.padding,
            dilation=self.dilation,
            activation="Linear",
            rescaling="NoScaling")

        return list_configs

    def forward(self, list_actions:list):

        if not self.is_last:
            list_actions.append(set_up_output(self.name, "float"))

        list_actions.append(generate_str(
            str(ROOT / "templates" / "kernel_forward" / "convolution_forward.jinja"),
            name=self.name,
            input_name=self.inputs[0].name(),
            output_name=self.name,
            weights_name=self.inputs[1].name(),
            biases_name=self.inputs[2].name()
        ))
        return list_actions


@operator_register("PaddedConv")
class PaddedConvCPP(ConvCPP):
    def __init__(self, node):
        ExportNode.__init__(self, node)

        for n in self.operator.get_micro_graph().get_nodes():
            if n.type() == "Pad":
                self.padding = n.get_operator().get_attr("BeginEndBorders")
            if n.type() == "Conv":
                self.kernel = n.get_operator().get_attr("KernelDims")
                self.stride = n.get_operator().get_attr("StrideDims")
                self.dilation = n.get_operator().get_attr("DilationDims")

        if len(self.inputs_dims[0]) == 4:
            # if dims == [batch, nb_channels, height, width]
            # transform to [nb_channels, height, width]
            self.inputs_dims[0] = self.inputs_dims[0][1:]

        if len(self.outputs_dims[0]) == 4:
            # if dims == [batch, nb_outputs]
            # transform to [nb_outputs, 1, 1]
            self.outputs_dims[0] = self.outputs_dims[0][1:]

@operator_register("Add")
class AddCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

    def export(self, export_folder:str, list_configs:list):
        list_configs.append(f"layers/{self.name}.h")
        list_configs.append("kernels/elemwise.hpp")

        copyfile(str(ROOT / "kernels" / "elemwise.hpp"),
                 str(export_folder / "include" / "kernels"))

        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "elemwise_config.jinja"),
            name=self.name,
            nb_elts=np.prod(self.inputs_dims[0]),
            activation="Linear",
            elemwise_op="Add",
            rescaling="NoScaling")

        return list_configs

    def forward(self, list_actions:list):

        list_actions.append(set_up_output(self.name, "float"))
        list_actions.append(generate_action(
            str(ROOT / "templates" / "kernel_forward" / "elemwise_forward.jinja"),
            name=self.name,
            inputs1_name=self.parents[0].name(),
            inputs2_name=self.parents[1].name(),
            output_name=self.name
        ))
        return list_actions

@operator_register("Sub")
class SubCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

    def export(self, export_folder:str, list_configs:list):
        list_configs.append(f"layers/{self.name}.h")
        list_configs.append("kernels/elemwise.hpp")
        copyfile(str(ROOT / "kernels" / "elemwise.hpp"),
                 str(export_folder / "include" / "kernels"))
        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "elemwise_config.jinja"),
            name=self.name,
            nb_elts=np.prod(self.inputs_dims[0]),
            activation="Linear",
            elemwise_op="Sub",
            rescaling="NoScaling")

        return list_configs

    def forward(self, list_actions:list):

        list_actions.append(set_up_output(self.name, "float"))
        list_actions.append(generate_action(
            str(ROOT / "templates" / "kernel_forward" / "elemwise_forward.jinja"),
            name=self.name,
            inputs1_name=self.inputs[0].name(),
            inputs2_name=self.inputs[1].name(),
            output_name=self.name
        ))
        return list_actions

@operator_register("MaxPooling")
class MaxPoolCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

        self.kernel = node.get_operator().get_attr("KernelDims")
        self.stride = node.get_operator().get_attr("StrideDims")

        # No padding with MaxPooling
        # Use PaddedMaxPooling to add padding attribute
        self.padding = [0, 0]

        if len(self.inputs_dims[0]) == 4:
            # if dims == [batch, nb_channels, height, width]
            # transform to [nb_channels, height, width]
            self.inputs_dims[0] = self.inputs_dims[0][1:]

        if len(self.outputs_dims[0]) == 4:
            # if dims == [batch, nb_outputs]
            # transform to [nb_outputs, 1, 1]
            self.outputs_dims[0] = self.outputs_dims[0][1:]

    def export(self, export_folder:Path, list_configs:list):

        copyfile(str(ROOT / "kernels" / "pooling.hpp"),
                 str(export_folder / "include" / "kernels"))

        list_configs.append("kernels/pooling.hpp")
        list_configs.append(f"layers/{self.name}.h")

        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "pooling_config.jinja"),
            name=self.name,
            input_dims=self.inputs_dims[0],
            output_dims=self.outputs_dims[0],
            kernel=self.kernel,
            stride=self.stride,
            padding=self.padding,
            pool_type="Max",
            activation="Linear")

        return list_configs

    def forward(self, list_actions:list):

        if not self.is_last:
            list_actions.append(set_up_output(self.name, "float"))

        list_actions.append(generate_str(
            str(ROOT / "templates" / "kernel_forward" / "pooling_forward.jinja"),
            name=self.name,
            input_name=self.inputs[0].name(),
            output_name=self.name
        ))
        return list_actions

@operator_register("FC")
class FcCPP(ExportNode):
    def __init__(self, node):
        super().__init__(node)

        if len(self.inputs_dims[0]) == 4:
            # if dims == [batch, nb_channels, height, width]
            # transform to [nb_channels, height, width]
            self.inputs_dims[0] = self.inputs_dims[0][1:]
        elif len(self.inputs_dims[0]) == 2:
            # if dims == [batch, nb_channels]
            # transform to [nb_channels, 1, 1]
            self.inputs_dims[0] = [self.inputs_dims[0][1], 1, 1]

        if len(self.outputs_dims[0]) == 2:
            # if dims == [batch, nb_outputs]
            # transform to [nb_outputs, 1, 1]
            self.outputs_dims[0] = [self.outputs_dims[0][1], 1, 1]


    def export(self, export_folder:Path, list_configs:list):

        copyfile(str(ROOT / "kernels" / "fullyconnected.hpp"),
                 str(export_folder / "include" / "kernels"))
        copyfile(str(ROOT / "kernels" / "macs.hpp"),
                 str(export_folder / "include" / "kernels"))
        copyfile(str(ROOT / "kernels" / "activation.hpp"),
                 str(export_folder / "include" / "kernels"))

        # Add to config list the include of configurations
        list_configs.append("kernels/fullyconnected.hpp")
        list_configs.append(f"layers/{self.name}.h")

        # Export configuration file
        generate_file(
            str(export_folder / "layers" / f"{self.name}.h"),
            str(ROOT / "templates" / "configuration" / "fullyconnected_config.jinja"),
            name=self.name,
            input_dims=self.inputs_dims[0],
            output_dims=self.outputs_dims[0],
            activation="Linear",
            rescaling="NoScaling")

        return list_configs

    def forward(self, list_actions:list):
        if not self.is_last:
            list_actions.append(set_up_output(self.name, "float"))

        list_actions.append(generate_str(
            str(ROOT / "templates" / "kernel_forward" / "fullyconnected_forward.jinja"),
            name=self.name,
            inputs_name=self.inputs[0].name(),
            weights_name=self.inputs[1].name(),
            biases_name=self.inputs[2].name(),
            outputs_name=self.name
        ))
        return list_actions






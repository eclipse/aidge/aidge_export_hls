import re
import os
from pathlib import Path
import shutil
import numpy as np
from typing import List, Union
from jinja2 import Environment, FileSystemLoader

import aidge_core
from aidge_core.export.code_generation import *
from aidge_export_cpp.utils import (ROOT, OPERATORS_REGISTRY, supported_operators)
from aidge_export_cpp.utils.converter import aidge_datatype2ctype, numpy_dtype2ctype
import aidge_export_cpp.operators
from aidge_export_cpp.utils.generation import *
from aidge_export_cpp.memory import *


def generate_input_file(export_folder:str,
                        array_name:str,
                        array: np.ndarray):
    
    # If directory doesn't exist, create it
    if not os.path.exists(export_folder):
        os.makedirs(export_folder)

    generate_file(
        file_path=f"{export_folder}/{array_name}.h",
        template_path=str(ROOT / "templates" / "data" / "inputs.jinja"),
        dims = array.shape,
        data_t = numpy_dtype2ctype(array.dtype),
        name = array_name,
        values = array.tolist()
    )


def export(export_folder_name, graphview, scheduler):

    export_folder = Path().absolute() / export_folder_name

    os.makedirs(str(export_folder), exist_ok=True)

    dnn_folder = export_folder / "dnn"
    os.makedirs(str(dnn_folder), exist_ok=True)

    list_actions = []
    list_configs = []

    list_forward_nodes = scheduler.get_static_scheduling()

    for node in list_forward_nodes:
        if node.type() in supported_operators():
            op = OPERATORS_REGISTRY[node.type()](node)
            
            # For configuration files
            list_configs = op.export(dnn_folder, list_configs)

            # For forward file
            list_actions = op.forward(list_actions)


    # Memory management
    mem_size, mem_info = compute_default_mem_info(scheduler)

    # Generate the memory file
    generate_file(
        str(dnn_folder / "memory" / "mem_info.h"),
        str(ROOT / "templates" / "memory" / "mem_info.jinja"),
        mem_size = mem_size,
        mem_info_legends = MEMORY_INFO_TEMPLATE,
        mem_info = mem_info
    )
    list_configs.append("memory/mem_info.h")

    # Get entry nodes
    # It supposes the entry nodes are producers with constant=false
    # Store the datatype & name
    list_inputs_name = []
    for node in graphview.get_nodes():
        if node.type() == "Producer":
            if not node.get_operator().get_attr("Constant"):
                export_type = aidge_datatype2ctype(node.get_operator().get_output(0).dtype())
                list_inputs_name.append((export_type, node.name()))

    # Get output nodes
    # Store the datatype & name, like entry nodes
    list_outputs_name = []
    for node in graphview.get_nodes():
        if len(node.get_children()) == 0:
            export_type = aidge_datatype2ctype(node.get_operator().get_output(0).dtype())
            list_outputs_name.append((export_type, node.name()))

    # Generate forward file
    generate_file(
        str(dnn_folder / "src" / "forward.cpp"),
        str(ROOT / "templates" / "network" / "network_forward.jinja"),
        headers=list_configs,
        actions=list_actions,
        inputs= list_inputs_name,
        outputs=list_outputs_name
    )

    # Generate dnn API
    generate_file(
        str(dnn_folder / "include" / "dnn.hpp"),
        str(ROOT / "templates" / "network" / "dnn_header.jinja"),
        libraries=[],
        functions=get_functions_from_c_file(str(dnn_folder / "src" / "forward.cpp")),
    )

    # Copy all static files in the export
    shutil.copy(str(ROOT / "static" / "main.cpp"), str(export_folder))
    shutil.copy(str(ROOT / "static" / "Makefile"), str(export_folder))
    shutil.copytree(str(ROOT / "static" / "include"), str(dnn_folder / "include"), dirs_exist_ok=True)
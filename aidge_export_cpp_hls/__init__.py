r"""
Aidge Export for CPP standalone projects

"""

from .operators import *
from collections import defaultdict
import aidge_core

from aidge_export_cpp.utils import ROOT

__version__ = open(ROOT / "version.txt", "r").read().strip()

from .export import *


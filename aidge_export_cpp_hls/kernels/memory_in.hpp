#ifndef N2D2_MEMORY_IN
#define N2D2_MEMORY_IN

#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <limits>
#include <stdexcept>
#include <type_traits>
#include <chrono>
#include <map>
#include <numeric>

#include "typedefs.h"
#include "ac_channel.h"
#include "ac_sync.h"
#include "mem_info.hpp"
#include "layers.hpp"
#include "mc_scverify.h"


template <int NB_CHANNELS,
            int CHANNELS_HEIGHT, int CHANNELS_WIDTH,
            int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
            int PADDING_Y, int PADDING_X,
            int STRIDE_Y, int STRIDE_X,
            int KERNEL_HEIGHT, int KERNEL_WIDTH,
            // Memory mapping: inputs
            int INPUT_MEM_CONT_OFFSET,
            int INPUT_MEM_CONT_SIZE,
            int INPUT_MEM_WRAP_OFFSET,
            int INPUT_MEM_WRAP_SIZE,
            int INPUT_MEM_STRIDE,
            typename Input_T, typename OutputStruct_T>
class mem_in {
public:
    mem_in() {}

    #pragma hls_design interface
    void run(ac_sync &start,
                const Input_T inputs[CHANNELS_HEIGHT * CHANNELS_WIDTH * NB_CHANNELS],
                ac_channel<OutputStruct_T> &outputs)
    {
        start.sync_in();
        constexpr int OUTPUTS_HEIGHT_NOPAD = (CHANNELS_HEIGHT - KERNEL_HEIGHT + STRIDE_Y) / STRIDE_Y;
        constexpr int OUTPUTS_WIDTH_NOPAD = (CHANNELS_WIDTH - KERNEL_WIDTH + STRIDE_X) / STRIDE_X;

        OUTPUTS_HEIGHT_MEM:
        for (int oy = 0; oy < OUTPUTS_HEIGHT; ++oy)
        {
            const int syMin = (PADDING_Y == 0) ? 0: max(PADDING_Y - (oy * STRIDE_Y), 0);
            const int syMax = (PADDING_Y == 0 && OUTPUTS_HEIGHT == OUTPUTS_HEIGHT_NOPAD) ? KERNEL_HEIGHT
                                                                                            : clamp(CHANNELS_HEIGHT + PADDING_Y - (oy * STRIDE_Y),
                                                                                                    0, KERNEL_HEIGHT);
            const int iy = (oy * STRIDE_Y) - PADDING_Y;

        OUTPUTS_WIDTH_MEM:
            for (int ox = 0; ox < OUTPUTS_WIDTH; ++ox)
            {   
                OutputStruct_T output_data;

                const int sxMin = (PADDING_X == 0) ? 0 : max(PADDING_X - (ox * STRIDE_X), 0);
                const int sxMax = (PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) ? KERNEL_WIDTH : clamp(CHANNELS_WIDTH + PADDING_X - (ox * STRIDE_X), 0, KERNEL_WIDTH);
                const int ix = (ox * STRIDE_X) - PADDING_X;

                KERNEL_H_MEM:
                for (int sy = 0; sy < KERNEL_HEIGHT; ++sy)
                {
                    if ((PADDING_Y != 0 || OUTPUTS_HEIGHT != OUTPUTS_HEIGHT_NOPAD) && sy >= syMax - syMin)
                    {
                        break;
                    }

                    const int iPos = ((sxMin + ix) + CHANNELS_WIDTH * (iy + syMin + sy));
                    int iOffset = INPUT_MEM_STRIDE * iPos; 

                    bool wrapInRange = false;

                    if (!wrapInRange && (NB_CHANNELS == INPUT_MEM_STRIDE && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) || sxMax - sxMin == KERNEL_WIDTH)))
                    {
                    KERNEL_WCH_NB_MEM:
                        for (int iter = 0; iter < KERNEL_WIDTH * NB_CHANNELS; ++iter) 
                        {
                            output_data.mem[iter + sy * KERNEL_HEIGHT * NB_CHANNELS] = inputs[iOffset + iter];    
                        }
                    }

                    else {

                        KERNEL_W_NB_MEM:
                        for (int i = 0; i < KERNEL_WIDTH * NB_CHANNELS; ++i) {
                            int sx = i / NB_CHANNELS;
                            int idx = i % NB_CHANNELS;
                            
                            if ((PADDING_X != 0 || OUTPUTS_WIDTH != OUTPUTS_WIDTH_NOPAD) && sx >= sxMax - sxMin) {
                                break;
                            }
                            
                            int iOffsetInRange = iOffset + sx * INPUT_MEM_STRIDE;
                            int memIndex = idx + sx * NB_CHANNELS + sy * KERNEL_HEIGHT * KERNEL_WIDTH;
                            output_data.mem[memIndex] = inputs[iOffsetInRange + idx];
                        }

                    }
                }
                outputs.write(output_data);
            }
        }
    }
};

#endif





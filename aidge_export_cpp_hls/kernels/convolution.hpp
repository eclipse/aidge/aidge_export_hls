#ifndef __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION__
#define __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION__

#include "network/typedefs.hpp"
#include "network/rescaling.hpp"
#include "network/utils.hpp"
#include "kernels/macs.hpp"
#include "kernels/activation.hpp"


template<int NB_CHANNELS, 
         int CHANNELS_HEIGHT, int CHANNELS_WIDTH,
         int NB_OUTPUTS,
         int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
         int PADDING_Y, int PADDING_X,
         int STRIDE_Y, int STRIDE_X,
         int DILATION_Y, int DILATION_X,
         int KERNEL_HEIGHT, int KERNEL_WIDTH,
         ActivationFunction_T ACTIVATION,
         typename Input_T, typename Output_T, 
         typename Weight_T, typename Bias_T,
         typename Rescaling_T>
    inline void convolution_forward(
    // ac_channel for input and output data
    ac_channel<Input_T> &inputs,
    ac_channel<PW_out_t> &outputs,
    const Weight_T weights[NB_CHANNELS * NB_OUTPUTS * KERNEL_HEIGHT * KERNEL_WIDTH],
    const Bias_T biases[NB_OUTPUTS],
    const Rescaling_T& __restrict rescaling) const

{
    constexpr int DILATED_KERNEL_HEIGHT 
            = KERNEL_HEIGHT + (DILATION_Y - 1) * (KERNEL_HEIGHT - 1);

    constexpr int DILATED_KERNEL_WIDTH 
            = KERNEL_WIDTH + (DILATION_X - 1) * (KERNEL_WIDTH - 1);

    constexpr int OUTPUTS_HEIGHT_NOPAD
        = (CHANNELS_HEIGHT - DILATION_Y * (KERNEL_HEIGHT - 1) - 1 + STRIDE_Y) / STRIDE_Y;
    constexpr int OUTPUTS_WIDTH_NOPAD
        = (CHANNELS_WIDTH - DILATION_X * (KERNEL_WIDTH - 1) - 1 + STRIDE_X) / STRIDE_X;

    OUTPUTS_HEIGHT_STD:
    for (int oy = 0; oy < OUTPUTS_HEIGHT; ++oy) {
        const int syMin = (PADDING_Y == 0) ? 0
            : max(PADDING_Y - (oy * STRIDE_Y), 0);
        const int syMax = (PADDING_Y == 0
                && OUTPUTS_HEIGHT == OUTPUTS_HEIGHT_NOPAD) ? DILATED_KERNEL_HEIGHT
            : clamp(CHANNELS_HEIGHT + PADDING_Y - (oy * STRIDE_Y), 
                    0, DILATED_KERNEL_HEIGHT);
        const int iy = (oy * STRIDE_Y) - PADDING_Y;

        OUTPUTS_WIDTH_STD:
        for (int ox = 0; ox < OUTPUTS_WIDTH; ++ox) {

            /* Reading input data => reading X pixels */
            Input_T input_data = inputs.read();  

            NB_OUTPUTS_STD:
            for (int output = 0; output < NB_OUTPUTS; ++output) {
                // moved to inner loop for collapsing -->
                const int sxMin = (PADDING_X == 0) ? 0
                    : max(PADDING_X - (ox * STRIDE_X), 0);
                const int sxMax = (PADDING_X == 0
                        && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD)
                            ? DILATED_KERNEL_WIDTH
                    : clamp(CHANNELS_WIDTH + PADDING_X - (ox * STRIDE_X), 
                            0, DILATED_KERNEL_WIDTH);
                const int ix = (ox * STRIDE_X) - PADDING_X;

                const int oPos = (ox + OUTPUTS_WIDTH * oy);
                int oOffset = NB_OUTPUTS * oPos;

                // <--

                Bias_T weightedSum = biases[output];

                KERNEL_H_STD:
                for (int sy = 0; sy < KERNEL_HEIGHT; ++sy) {
                    if ((PADDING_Y != 0
                            || OUTPUTS_HEIGHT != OUTPUTS_HEIGHT_NOPAD)
                        && ((sy*DILATION_Y < syMin) || (sy*DILATION_Y >= syMax)))
                    {
                        continue;
                    }

                    const int iPos = ix + CHANNELS_WIDTH * (iy + sy*DILATION_Y);
                    int iOffset = NB_CHANNELS * iPos;

                    const int wOffset = (output*KERNEL_HEIGHT + sy) * KERNEL_WIDTH * NB_CHANNELS;

                    if (DILATION_X == 1 && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD)
                        || sxMax - sxMin == KERNEL_WIDTH))
                    {
                        KERNEL_W_CH_STD:
                        for (int iter = 0; iter < KERNEL_WIDTH * NB_CHANNELS; ++iter){ 
                            weightedSum += input_data.mem[iter + sy * KERNEL_HEIGHT * NB_CHANNELS] * weights[wOffset + iter];
                        }
                        
                    }
                    else {
                        KERNEL_W_Edge_STD:
                        for (int sx = 0; sx < KERNEL_WIDTH; ++sx) {
                            if ((PADDING_X != 0
                                    || OUTPUTS_WIDTH != OUTPUTS_WIDTH_NOPAD)
                                && ((sx*DILATION_X < sxMin) || (sx*DILATION_X >= sxMax)))
                            {
                                continue;
                            }

                            int iOffsetInRange = iOffset
                                + sx * DILATION_X * NB_CHANNELS;

                            NB_CHANNELS_STD:
                            for (int idx = 0; idx < NB_CHANNELS; ++idx){ 
                                weightedSum += input_data.mem[idx + sx * NB_CHANNELS + sy*KERNEL_HEIGHT*KERNEL_WIDTH] * weights[wOffset + sx*NB_CHANNELS + idx];
                            }
                        }
                    }
                }
                PW_out_t output_data;
                output_data.data = activation_forward_value<Output_T>(weightedSum, output, ACTIVATION, rescaling);
                if((oy == OUTPUTS_HEIGHT-1) && (ox == OUTPUTS_WIDTH -1) && (output == 0))
                    output_data.flag = true; // allow next block memory reads to start
                else if((oy == OUTPUTS_HEIGHT-1) && (ox == OUTPUTS_WIDTH -1) && (output == NB_OUTPUTS-1))
                    output_data.flag = true; // indicate last data for next block to reinit
                else
                    output_data.flag = false;
                outputs.write(output_data);

                // outputs[oOffset + output] = activation_forward_value<Output_T>(weightedSum, output, ACTIVATION, rescaling);
            }
        }
    }
}

#endif  // __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION__

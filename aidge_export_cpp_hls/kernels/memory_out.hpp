#ifndef N2D2_MEMORY_OUT
#define N2D2_MEMORY_OUT

#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <limits>
#include <stdexcept>
#include <type_traits>
#include <chrono>
#include <map>
#include <numeric>

#include "typedefs.h"
#include "ac_channel.h"
#include "ac_sync.h"
#include "mem_info.hpp"
#include "layers.hpp"
#include "mc_scverify.h"

template <int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH, int NB_OUTPUTS, typename Output_T>
class mem_out {
public:
    mem_out() {}

    #pragma hls_design interface
    void run(ac_channel<PW_out_t> &inputs,
                Output_T outputs[OUTPUTS_HEIGHT * OUTPUTS_WIDTH * NB_OUTPUTS])
    {
        OUTPUTS:
        for (int o = 0; o < OUTPUTS_HEIGHT * OUTPUTS_WIDTH * NB_OUTPUTS; o++)
        {
            PW_out_t input_data = inputs.read();
            outputs[o] = input_data.data;
        }
    }
}

#endif
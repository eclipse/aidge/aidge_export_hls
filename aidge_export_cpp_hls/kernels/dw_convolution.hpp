#ifndef __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION_DW__
#define __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION_DW__

#include "network/typedefs.hpp"
#include "network/rescaling.hpp"
#include "network/utils.hpp"
#include "kernels/macs.hpp"
#include "kernels/activation.hpp"

#pragma hls_design
    template <int NB_CHANNELS, 
    int CHANNELS_HEIGHT, int CHANNELS_WIDTH,
    int NB_OUTPUTS,
    int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
    int PADDING_Y, int PADDING_X,
    int STRIDE_Y, int STRIDE_X,
    int DILATION_Y, int DILATION_X,
    int KERNEL_HEIGHT, int KERNEL_WIDTH,
    ActivationFunction_T ACTIVATION,
    typename Input_T, typename Output_T, 
    typename Weight_T, typename Bias_T,
    typename Rescaling_T>
    void convolution_dw_forward(
    // ac_channel is used for input and output data
    ac_channel<Input_T> &inputs,
    ac_channel<OutputStruct_T> &outputs,
    const Bias_T biases[NB_OUTPUTS],
    const Weight_T weights[NB_OUTPUTS * KERNEL_HEIGHT * KERNEL_WIDTH],
    const Rescaling_T &__restrict rescaling) const
    {
        
    static_assert(NB_OUTPUTS % NB_CHANNELS == 0, "NB_OUTPUTS should be a multiple of NB_CHANNELS.");
    constexpr int OUTPUTS_HEIGHT_NOPAD = (CHANNELS_HEIGHT - KERNEL_HEIGHT + STRIDE_Y) / STRIDE_Y;
    constexpr int OUTPUTS_WIDTH_NOPAD = (CHANNELS_WIDTH - KERNEL_WIDTH + STRIDE_X) / STRIDE_X;
    
        OUTPUTS_HEIGHT_DW:
            for (int oy = 0; oy < OUTPUTS_HEIGHT; ++oy)
            {
                const int syMin = (PADDING_Y == 0) ? 0
                                                    : max(PADDING_Y - (oy * STRIDE_Y), 0);
                const int syMax = (PADDING_Y == 0 && OUTPUTS_HEIGHT == OUTPUTS_HEIGHT_NOPAD) ? KERNEL_HEIGHT
                                                                                                : clamp(CHANNELS_HEIGHT + PADDING_Y - (oy * STRIDE_Y),
                                                                                                        0, KERNEL_HEIGHT);
                const int iy = (oy * STRIDE_Y) - PADDING_Y;
        
            OUTPUTS_WIDTH_DW:
                for (int ox = 0; ox < OUTPUTS_WIDTH; ++ox)
                {
                    OutputStruct_T output_data;
        
                    NB_OUTPUTS_DW: 
                    for (int output = 0; output < NB_OUTPUTS; ++output)
                    {
                        // moved to inner loop for collapsing -->
                        const int sxMin = (PADDING_X == 0) ? 0
                                                            : max(PADDING_X - (ox * STRIDE_X), 0);
                        const int sxMax = (PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD)
                                                ? KERNEL_WIDTH
                                                : clamp(CHANNELS_WIDTH + PADDING_X - (ox * STRIDE_X),
                                                        0, KERNEL_WIDTH);
        
                        const int ix = (ox * STRIDE_X) - PADDING_X;
                        const int oPos = (ox + OUTPUTS_WIDTH * oy);
                        int oOffset = NB_OUTPUTS * oPos;
        
                        if (OUTPUT_MEM_WRAP_SIZE > 0 && oOffset >= OUTPUT_MEM_CONT_SIZE)
                        {
                            oOffset += OUTPUT_MEM_WRAP_OFFSET - OUTPUT_MEM_CONT_OFFSET - OUTPUT_MEM_CONT_SIZE;
                        }
                        // <--
        
                        const int channel = (output * NB_CHANNELS) / NB_OUTPUTS;
        
                        Bias_T weightedSum = biases[output];
        
                    KERNEL_H_DW:
                        for (int sy = 0; sy < KERNEL_HEIGHT; ++sy)
                        {
                             if ((PADDING_Y != 0
                            || OUTPUTS_HEIGHT != OUTPUTS_HEIGHT_NOPAD)
                                && ((sy*DILATION_Y < syMin) || (sy*DILATION_Y >= syMax)))
                            {
                                continue;
                            }
        
                            const int iPos = ix + CHANNELS_WIDTH * (iy + sy*DILATION_Y);
                            int iOffset = NB_CHANNELS * iPos;
        
                            const int wOffset = (sxMin + KERNEL_WIDTH * (syMin + sy + KERNEL_HEIGHT * output));
        
                            if (DILATION_X == 1 && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) || sxMax - sxMin == KERNEL_WIDTH))
                            {
                            KERNEL_W_DW:
                                for (int iter = 0; iter < KERNEL_WIDTH; ++iter)
                                {
                                    weightedSum += inputs.read() * weights[wOffset + iter];
                                }
                            }
                            else
                            {
                            KERNEL_W_DW_edge:
                                for (int sx = 0; sx < KERNEL_WIDTH; ++sx)
                                {
                                    if ((PADDING_X != 0
                                    || OUTPUTS_WIDTH != OUTPUTS_WIDTH_NOPAD)
                                    && ((sx*DILATION_X < sxMin) || (sx*DILATION_X >= sxMax)))
                                    {
                                        continue;
                                    }
                                    weightedSum += inputs.read() * weights[wOffset + sx];
                                    
                                }
                            }
                        }
                        output_data.mem[output] = activation_forward_value<Output_T>(weightedSum, output, ACTIVATION, rescaling);
                    }
                    outputs.write(output_data);
                }
            }
        };

#endif
#ifndef __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION_DW__
#define __AIDGE_EXPORT_CPP_KERNELS_CONVOLUTION_DW__

#include "network/typedefs.hpp"
#include "network/rescaling.hpp"
#include "network/utils.hpp"
#include "kernels/macs.hpp"
#include "kernels/activation.hpp"


#pragma hls_design
template <int NB_CHANNELS, 
        int CHANNELS_HEIGHT, int CHANNELS_WIDTH,
        int NB_OUTPUTS,
        int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
        int PADDING_Y, int PADDING_X,
        int STRIDE_Y, int STRIDE_X,
        int DILATION_Y, int DILATION_X,
        int KERNEL_HEIGHT, int KERNEL_WIDTH,
        ActivationFunction_T ACTIVATION,
        typename Input_T, typename Output_T, 
        typename Weight_T, typename Bias_T,
        typename Rescaling_T>
        void convolution_pw_forward(
        // ac_channel is used for input and output data
        ac_channel<Input_T> &inputs,
        ac_channel<PW_out_t> &outputs,
        const Bias_T biases[NB_OUTPUTS],
        const Weight_T weights[NB_OUTPUTS * NB_CHANNELS * KERNEL_HEIGHT * KERNEL_WIDTH],
        const Rescaling_T &__restrict rescaling) const
    {
    
        constexpr int OUTPUTS_HEIGHT_NOPAD = (CHANNELS_HEIGHT - KERNEL_HEIGHT + STRIDE_Y) / STRIDE_Y;
        constexpr int OUTPUTS_WIDTH_NOPAD = (CHANNELS_WIDTH - KERNEL_WIDTH + STRIDE_X) / STRIDE_X;

    OUTPUTS_HEIGHT_PW:
        for (int oy = 0; oy < OUTPUTS_HEIGHT; ++oy)
        {
            const int syMin = (PADDING_Y == 0) ? 0
                                                : max(PADDING_Y - (oy * STRIDE_Y), 0);
            const int syMax = (PADDING_Y == 0 && OUTPUTS_HEIGHT == OUTPUTS_HEIGHT_NOPAD) ? KERNEL_HEIGHT
                                                                                            : clamp(CHANNELS_HEIGHT + PADDING_Y - (oy * STRIDE_Y),
                                                                                                    0, KERNEL_HEIGHT);
            const int iy = (oy * STRIDE_Y) - PADDING_Y;
    
        OUTPUTS_WIDTH_PW:
            for (int ox = 0; ox < OUTPUTS_WIDTH; ++ox)
            {
                const int sxMin = (PADDING_X == 0) ? 0
                                                    : max(PADDING_X - (ox * STRIDE_X), 0);
                const int sxMax = (PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD)
                                        ? KERNEL_WIDTH
                                        : clamp(CHANNELS_WIDTH + PADDING_X - (ox * STRIDE_X),
                                                0, KERNEL_WIDTH);
                const int ix = (ox * STRIDE_X) - PADDING_X;
    
                const int oPos = (ox + OUTPUTS_WIDTH * oy);
                int oOffset = OUTPUT_MEM_STRIDE * oPos;
    
                Input_T input_data = inputs.read();

            NB_OUTPUTS_PW: // pipelined with II=1
                for (int output = 0; output < NB_OUTPUTS; ++output)
                {
    
                    if (OUTPUT_MEM_WRAP_SIZE > 0 && oOffset >= OUTPUT_MEM_CONT_SIZE)
                    {
                        oOffset += OUTPUT_MEM_WRAP_OFFSET - OUTPUT_MEM_CONT_OFFSET - OUTPUT_MEM_CONT_SIZE;
                    }
    
                    SUM_T weightedSum = biasses[output];
    
                KERNEL_H_PW:
                    for (int sy = 0; sy < KERNEL_HEIGHT; ++sy)
                    {
                        if ((PADDING_Y != 0 || OUTPUTS_HEIGHT != OUTPUTS_HEIGHT_NOPAD) && sy >= syMax - syMin)
                        {
                            break;
                        }
    
                        const int iPos = ((sxMin + ix) + CHANNELS_WIDTH * (iy + syMin + sy));
                        int iOffset = INPUT_MEM_STRIDE * iPos;
    
                        // Wrapping cannot occur in the middle of a line, except if
                        // there is only one line (1D)!
                        bool wrapInRange = false;
    
                        if (INPUT_MEM_WRAP_SIZE > 0 && iOffset >= INPUT_MEM_CONT_SIZE)
                        {
                            iOffset += INPUT_MEM_WRAP_OFFSET - INPUT_MEM_CONT_OFFSET - INPUT_MEM_CONT_SIZE;
                        }
                        else if (INPUT_MEM_WRAP_SIZE > 0 && KERNEL_WIDTH > 1 && CHANNELS_HEIGHT == 1 // single line (1D)!
                                    && iOffset + KERNEL_WIDTH * NB_CHANNELS > INPUT_MEM_CONT_SIZE)
                        {
                            wrapInRange = true;
                        }
    
                        const int wOffset = NB_CHANNELS * (sxMin + KERNEL_WIDTH * (syMin + sy + KERNEL_HEIGHT * output));
    
                        if (!wrapInRange && (NB_CHANNELS == INPUT_MEM_STRIDE && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) || sxMax - sxMin == KERNEL_WIDTH)))
                        {
                        KERNEL_W_NB_PW:
                            for (int iter = 0; iter < KERNEL_WIDTH * NB_CHANNELS; ++iter)
                            {
                                weightedSum += input_data.mem[iter] * weights[wOffset + iter]; 
                            }
                        }
                    }
                    PW_out_t output_data;
                    output_data.data = sat<Output_T>(weightedSum, output, ACTIVATION, rescaling);
                    if((oy == OUTPUTS_HEIGHT-1) && (ox == OUTPUTS_WIDTH -1) && (output == 0))
                        output_data.flag = true; // allow next block memory reads to start
                    else if((oy == OUTPUTS_HEIGHT-1) && (ox == OUTPUTS_WIDTH -1) && (output == NB_OUTPUTS-1))
                        output_data.flag = true; // indicate last data for next block to reinit
                    else
                        output_data.flag = false;
                    outputs.write(output_data);
                }
            }
        }
    }

#endif
#ifndef N2D2_MEMORY_PW_DW
#define N2D2_MEMORY_PW_DW

#include <cassert>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <limits>
#include <stdexcept>
#include <type_traits>
#include <chrono>
#include <map>
#include <numeric>

#include "typedefs.h"
#include "ac_channel.h"
#include "ac_sync.h"
#include "mem_info.hpp"
#include "layers.hpp"
#include "mc_scverify.h"


template <int NB_CHANNELS,
              int CHANNELS_HEIGHT, int CHANNELS_WIDTH,
              int NB_OUTPUTS,
              int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
              int PADDING_Y, int PADDING_X,
              int STRIDE_Y, int STRIDE_X,
              int KERNEL_HEIGHT, int KERNEL_WIDTH,
              int INPUT_MEM_CONT_OFFSET,
              int INPUT_MEM_CONT_SIZE,
              int INPUT_MEM_WRAP_OFFSET,
              int INPUT_MEM_WRAP_SIZE,
              int INPUT_MEM_STRIDE,
              typename Output_T>
    class PW_DW {
         Output_T mem[NB_CHANNELS*CHANNELS_HEIGHT*CHANNELS_WIDTH];
         bool mem_read_started, last_mem_write_done;
         uint8_t sx, sy, ox, oy;
         uint16_t output;
         uint32_t wr_addr;

    public:
        PW_DW() {
            mem_read_started = false;
            last_mem_write_done = false;
            oy = 0; ox = 0;
            output = 0;
            sy = 0; sx = 0;
            wr_addr = 0;
        }

        #pragma hls_design interface
        bool run(
            ac_channel<PW_out_t> &inputs,
            ac_channel<Output_T> &outputs)
        {
            bool done = false;
            bool mem_write = false;
            PW_out_t mem_wr_req;
            if(!last_mem_write_done) {
                mem_write = inputs.nb_read(mem_wr_req);
            }
            if(mem_write) {
                mem[wr_addr++] = mem_wr_req.data;
                if(mem_wr_req.flag) {
                    if(!mem_read_started) mem_read_started = true;
                    else {
                        last_mem_write_done = true;
                        wr_addr = 0;
                    }
                }
            }
            else if(mem_read_started) {
                static_assert(NB_OUTPUTS % NB_CHANNELS == 0, "NB_OUTPUTS should be a multiple of NB_CHANNELS.");
                constexpr int OUTPUTS_HEIGHT_NOPAD = (CHANNELS_HEIGHT - KERNEL_HEIGHT + STRIDE_Y) / STRIDE_Y;
                constexpr int OUTPUTS_WIDTH_NOPAD = (CHANNELS_WIDTH - KERNEL_WIDTH + STRIDE_X) / STRIDE_X;
            
                const int syMin = (PADDING_Y == 0) ? 0
                                                   : max(PADDING_Y - (oy * STRIDE_Y), 0);
                const int syMax = (PADDING_Y == 0 && OUTPUTS_HEIGHT == OUTPUTS_HEIGHT_NOPAD) ? KERNEL_HEIGHT
                                                                                             : clamp(CHANNELS_HEIGHT + PADDING_Y - (oy * STRIDE_Y),
                                                                                                     0, KERNEL_HEIGHT);
                const int iy = (oy * STRIDE_Y) - PADDING_Y;
            
                const int sxMin = (PADDING_X == 0) ? 0
                                                   : max(PADDING_X - (ox * STRIDE_X), 0);
                const int sxMax = (PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD)
                                      ? KERNEL_WIDTH
                                      : clamp(CHANNELS_WIDTH + PADDING_X - (ox * STRIDE_X),
                                              0, KERNEL_WIDTH);
            
                const int ix = (ox * STRIDE_X) - PADDING_X;
                const int channel = (output * NB_CHANNELS) / NB_OUTPUTS;
            
                const int iPos = ((sxMin + ix) + CHANNELS_WIDTH * (iy + syMin + sy));
                int iOffset = INPUT_MEM_STRIDE * iPos;
            
                // Wrapping cannot occur in the middle of a line, except if
                // there is only one line (1D)!
                bool wrapInRange = false;
            
                if (INPUT_MEM_WRAP_SIZE > 0 && iOffset >= INPUT_MEM_CONT_SIZE)
                {
                    iOffset += INPUT_MEM_WRAP_OFFSET - INPUT_MEM_CONT_OFFSET - INPUT_MEM_CONT_SIZE;
                }
                else if (INPUT_MEM_WRAP_SIZE > 0 && KERNEL_WIDTH > 1 && CHANNELS_HEIGHT == 1 // single line (1D)!
                         && iOffset + KERNEL_WIDTH * INPUT_MEM_STRIDE > INPUT_MEM_CONT_SIZE)
                {
                    wrapInRange = true;
                }
                uint32_t mem_addr_read;
                if (!wrapInRange && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) || sxMax - sxMin == KERNEL_WIDTH))
                {
#ifndef __SYNTHESIS__
                    assert(iOffset + channel + (sx * INPUT_MEM_STRIDE) >= 0);
#endif
                    mem_addr_read = iOffset + channel + (sx * INPUT_MEM_STRIDE);
                  //mem_addr_read = iPos + sx + output * CHANNELS_HEIGHT * CHANNELS_WIDTH;
                }
                else
                {
                    int iOffsetInRange = iOffset + sx * INPUT_MEM_STRIDE;
            
                    if (wrapInRange &&
                        iOffsetInRange >= INPUT_MEM_CONT_SIZE)
                    {
                        iOffsetInRange += INPUT_MEM_WRAP_OFFSET - INPUT_MEM_CONT_OFFSET - INPUT_MEM_CONT_SIZE;
                    }
#ifndef __SYNTHESIS__
                    assert(iOffsetInRange + channel >= 0);
#endif
                    mem_addr_read = iOffsetInRange + channel;
                  //mem_addr_read = iPos + sx + output * CHANNELS_HEIGHT * CHANNELS_WIDTH;
                }
                outputs.write(mem[mem_addr_read]);
                sx++;
                if(sx==KERNEL_WIDTH || (!(!wrapInRange && ((PADDING_X == 0 && OUTPUTS_WIDTH == OUTPUTS_WIDTH_NOPAD) || sxMax - sxMin == KERNEL_WIDTH)) && ((PADDING_X != 0 || OUTPUTS_WIDTH != OUTPUTS_WIDTH_NOPAD) && sx >= sxMax - sxMin))) {
                    sx=0;
                    sy++;
                    if(sy==KERNEL_HEIGHT || ((PADDING_Y != 0 || OUTPUTS_HEIGHT != OUTPUTS_HEIGHT_NOPAD) && sy >= syMax - syMin)) {
                        sy=0;
                        output++;
                        if(output==NB_OUTPUTS) {
                            output=0;
                            ox++;
                            if(ox==OUTPUTS_WIDTH) {
                                ox=0;
                                oy++;
                                if(oy==OUTPUTS_HEIGHT) {
                                    oy=0;
                                    mem_read_started = false;
                                    last_mem_write_done = false;
                                    done = true;
                                }
                            }
                        }
                    }
                }
            }
            return(done);
        }
    }

#endif
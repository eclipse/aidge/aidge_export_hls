#!/usr/bin/env python3
""" Aidge CPP generic export
"""

DOCLINES = (__doc__ or '').split("\n")

import sys

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
Intended Audience :: Education
Intended Audience :: Science/Research
License :: OSI Approved :: Eclipse Public License 2.0 (EPL-2.0)
Programming Language :: C++
Programming Language :: Python
Programming Language :: Python :: 3
Programming Language :: Python :: 3.7
Programming Language :: Python :: 3.8
Programming Language :: Python :: 3.9
Programming Language :: Python :: 3 :: Only
Topic :: Scientific/Engineering
Topic :: Scientific/Engineering :: Artificial Intelligence
Topic :: Software Development
"""

import os
import shutil
import pathlib

from setuptools import setup, Extension
from setuptools import find_packages
from setuptools.command.build_ext import build_ext

def get_project_version() -> str:
    aidge_root = pathlib.Path().absolute()
    version = open(aidge_root / "version.txt", "r").read().strip()
    return version

class AdditionalExtension(Extension):
    def __init__(self, name):
        super().__init__(name, sources=[])

class AdditionalBuild(build_ext):

    def run(self):
        cwd = pathlib.Path().absolute()

        build_temp = cwd / "build"
        if not build_temp.exists():
            build_temp.mkdir(parents=True, exist_ok=True)

        build_lib = pathlib.Path(self.build_lib)
        if not build_lib.exists():
            build_lib.mkdir(parents=True, exist_ok=True)

        aidge_package = build_lib / "aidge_export_cpp"


        # Copy version.txt in aidge_package
        os.chdir(os.path.dirname(__file__))
        shutil.copy("version.txt", str(aidge_package.absolute()))


if __name__ == '__main__':

    setup(
        name="aidge_export_cpp",
        version=get_project_version(),
        license="Eclipse Public License 2.0 (EPL-2.0)",
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        platforms=["Linux"],
        packages=find_packages(where="."),
        include_package_data=True,
        ext_modules=[AdditionalExtension("aidge_export_cpp")],
        cmdclass={
            'build_ext': AdditionalBuild,
        },
    )

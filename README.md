# Aidge CPP HLS Export

Use this module to export your Aidge model to a CPP-HLS export.
This export is adapted to Catapult. 

## Project Structure

```bash
.
├── __init__.py
├── kernels         ----------------------> DNN kernels, each kernel is in a separate file
│   ├── activation.hpp
│   ├── convolution.hpp
│   ├── ...
│   └── 
├── operators.py    ----------------------> script to generate all the files of the export
├── register.py
├── static          ----------------------> static files that will be used as is in the export
│   ├── include
│   ├── main.cpp    ----------------------> TestBench file
│   └── Makefile
├── templates
│   ├── configuration  -------------------> jinja tempaltes for header files that will later contain weights and layers configurations
│   │   ├── activation_config.jinja
│   │   ├── convolution_config.jinja
│   │   ├── ...
│   │   └── 
│   ├── kernel_forward -------------------> jinja templates for layers instances
│   │   ├── activation_forward.jinja
│   │   ├── convolution_forward.jinja
│   │   ├── ...
│   │   └── 
│   └── network        -------------------> jinja templates for the header file of the whole DNN and the top function "model_forward" with the required function calls
│       ├── dnn_header.jinja
│       ├── environment.jinja
│       └── network_forward.jinja
└── unit_tests
    └── test_export.py
```

## Architecture Overview 
It is a class-based hierarchy that models the hardware that will be generated:
- Top class has a public function marked with \#pragma hls_design interface.
- Sub-blocks are:
    - private functions of the top class maked with \#pragma hls_design.
    - public functions of another class instance.
- Connections between blocks are AC channel instances that transfer data. 
- Arguments of functions modeling hierarchichal blocks become RTL ports.